/*
@author 2021 Éric Bart <bart.eric@hotmail.com>
 */

define(["local_fliplearning/vue",
        "local_fliplearning/vuetify",
        "local_fliplearning/axios",
        "local_fliplearning/moment",
        "local_fliplearning/pagination",
        "local_fliplearning/pageheader",
        "local_fliplearning/helpdialog",
        "local_fliplearning/alertify",
        
    ],
    function(Vue, Vuetify, Axios, Moment, Pagination, Pageheader, HelpDialog, Alertify) {
        "use strict";

        function init(content) {
            const timeout = 60 * 120 * 1000
            Axios.defaults.timeout = timeout
            Vue.use(Vuetify);
            Vue.component('pagination', Pagination);
            Vue.component('pageheader', Pageheader);
            Vue.component('helpdialog', HelpDialog);
            let vue = new Vue({
                delimiters: ["[[", "]]"],
                el: "#logs",
                vuetify: new Vuetify(),
                data() {
                    return {
                        calendarData: {},
                        strings : content.strings,
                        groups : content.groups,
                        userid : content.userid,
                        courseid : content.courseid,
                        timezone : content.timezone,
                        render_has : content.profile_render,
                        courseRole : content.courseRole,
                        loading : false,
                        errors : [],
                        pages : content.pages,
                        help_dialog: false,
                        help_contents: [],
                        dateRules: [
                            v => !!v || this.strings.logs_invalid_date
                        ]

                    }
                },
                beforeMount() {
                    document.querySelector("#downloadButtonMoodle").style.display = "none";
                    document.querySelector("#downloadButtonNMP").style.display = "none";
                },
                mounted(){
                    document.querySelector(".v-application--wrap").style.minHeight = "60vh";
                    document.querySelector("#sessions-loader").style.display = "none";
                    document.querySelector("#helpMoodle").style.display = "block";
                    document.querySelector("#helpNMP").style.display = "block";
                    document.querySelector("#downloadButtonMoodle").style.display = "block";
                    document.querySelector("#downloadButtonNMP").style.display = "block";
                },
                methods : {
                    get_Moodlefile() {
                        let lastDate = document.querySelector("#lastDateMoodle");
                        let beginDate = document.querySelector("#beginDateMoodle");
                        let timestampBeginDate = 0;
                        let timestampLastDate = 0;
                        let parsedBeginDate = [];
                        let parsedLastDate = [];
                        this.url = false;
                        this.loading = true;
                        var data = {
                            action : "downloadMOODLElogs",
                            courseid : this.courseid,
                            userid : this.userid,
                            beginDate : beginDate.value,
                            lastDate : lastDate.value,
                            currentUrl : window.location.href,
                        }
                        if(beginDate.value!="" && lastDate.value!="") {
                            parsedBeginDate = beginDate.value.split("-");
                            timestampBeginDate = new Date(parsedBeginDate[0], parsedBeginDate[1]-1, parsedBeginDate[2]);
                            parsedLastDate = lastDate.value.split("-");
                            timestampLastDate = new Date(parsedLastDate[0], parsedLastDate[1]-1, parsedLastDate[2]);
                            if(timestampBeginDate.getTime()<=timestampLastDate.getTime()) {
                                if(timestampBeginDate.getTime()<=Date.now()) {
                                    document.querySelector('#downloadButtonMoodle').innerHTML = this.strings.logs_download_btn;
                                    document.getElementById('downloadButtonMoodle').disabled = true;
                                    Axios({
                                        method: 'get',
                                        url: M.cfg.wwwroot + "/local/fliplearning/ajax.php",
                                        timeout: timeout,
                                        params: data,
                                    }).then((response) => {
                                        this.loading = false
                                        if (response.status == 200 && response.data.ok) {
                                                let path = M.cfg.wwwroot + "/local/fliplearning/downloads/"
                                                let url = path + response.data.data.filename;
                                                this.url = url
                                                var link = document.createElement('a');
                                                link.href = this.url;
                                                link.download = "MoodleLogs_" + beginDate.value + "_" + lastDate.value + ".csv";
                                                link.click();
                                                document.querySelector('#downloadButtonMoodle').innerHTML = this.strings.logs_valid_Moodlebtn;
                                                document.getElementById('downloadButtonMoodle').disabled = false;
                                                Alertify.success(this.strings.logs_success_file_downloaded);
                                        } else {
                                            Alertify.error(this.strings.logs_error_problem_encountered);
                                            document.querySelector('#downloadButtonMoodle').innerHTML = this.strings.logs_valid_Moodlebtn;
                                            document.getElementById('downloadButtonMoodle').disabled = false;
                                        }
                                    }).catch((e) => {
                                        Alertify.error(this.strings.logs_error_problem_encountered);
                                        this.loading = false;
                                        document.querySelector('#downloadButtonMoodle').innerHTML = this.strings.logs_valid_Moodlebtn;
                                        document.getElementById('downloadButtonMoodle').disabled = false;
                                    }).finally(() => {
                                        this.loading = false;
                                        document.querySelector('#downloadButtonMoodle').innerHTML = this.strings.logs_valid_Moodlebtn;
                                        document.getElementById('downloadButtonMoodle').disabled = false;
                                    });
                                } else {
                                    Alertify.error(this.strings.logs_error_begin_date_superior);
                                }
                            } else {
                                Alertify.error(this.strings.logs_error_begin_date_inferior);
                            }
                        } else {
                            Alertify.error(this.strings.logs_error_empty_dates);
                        }
                    },

                    getRapport() {
                        Alertify.confirm(this.strings.logs_download_details_description,
                        ()=> { //OK
                            let path = M.cfg.wwwroot + "/local/fliplearning/downloads/Details_Informations_LogsNMP.pdf";
                            var link = document.createElement('a');
                            link.href = path;
                            link.download = "Details_Informations_LogsNMP.pdf";
                            link.click(); 
                            Alertify.success(this.strings.logs_download_details_validation);
                        }).set({title: this.strings.logs_download_details_title})
                        .set({labels: {cancel: this.strings.logs_download_details_cancel, ok: this.strings.logs_download_details_ok}});
                    },

                    get_NMPfile() {
                        let lastDate = document.querySelector("#lastDateNMP");
                        let beginDate = document.querySelector("#beginDateNMP");
                        let timestampBeginDate = 0;
                        let timestampLastDate = 0;
                        let parsedBeginDate = [];
                        let parsedLastDate = [];
                        this.url = false;
                        this.loading = true;
                        var data = {
                            action : "downloadNMPlogs",
                            courseid : this.courseid,
                            userid : this.userid,
                            beginDate : beginDate.value,
                            lastDate : lastDate.value,
                            currentUrl : window.location.href,
                        }
                        if(beginDate.value!="" && lastDate.value!="") {
                            parsedBeginDate = beginDate.value.split("-");
                            timestampBeginDate = new Date(parsedBeginDate[0], parsedBeginDate[1] - 1, parsedBeginDate[2]);
                            parsedLastDate = lastDate.value.split("-");
                            timestampLastDate = new Date(parsedLastDate[0], parsedLastDate[1] - 1, parsedLastDate[2]);
                            if (timestampBeginDate.getTime() <= timestampLastDate.getTime()) {
                                if (timestampBeginDate.getTime() <= Date.now()) {
                                    document.querySelector('#downloadButtonNMP').innerHTML = this.strings.logs_download_btn;
                                    document.getElementById('downloadButtonNMP').disabled = true;
                                    Axios({
                                        method: 'get',
                                        url: M.cfg.wwwroot + "/local/fliplearning/ajax.php",
                                        timeout: timeout,
                                        params: data,
                                    }).then((response) => {
                                        this.loading = false
                                        if (response.status == 200 && response.data.ok) {
                                            if (beginDate.value != "" || lastDate.value != "") {
                                                let path = M.cfg.wwwroot + "/local/fliplearning/downloads/"
                                                let url = path + response.data.data.filename;
                                                this.url = url
                                                var link = document.createElement('a');
                                                link.href = this.url;
                                                link.download = "NMPLogs_" + beginDate.value + "_" + lastDate.value + ".csv";
                                                link.click();
                                                document.querySelector('#downloadButtonNMP').innerHTML = this.strings.logs_valid_NMPbtn;
                                                document.getElementById('downloadButtonNMP').disabled = false;
                                                Alertify.success(this.strings.logs_success_file_downloaded);
                                            }
                                        } else {
                                            Alertify.error(this.strings.logs_error_problem_encountered);
                                            document.querySelector('#downloadButtonNMP').innerHTML = this.strings.logs_valid_NMPbtn;
                                            document.getElementById('downloadButtonNMP').disabled = false;
                                        }
                                    }).catch((e) => {
                                        Alertify.error(this.strings.logs_error_problem_encountered);
                                        this.loading = false;
                                        document.querySelector('#downloadButtonNMP').innerHTML = this.strings.logs_valid_NMPbtn;
                                        document.getElementById('downloadButtonNMP').disabled = false;
                                    }).finally(() => {
                                        this.loading = false;
                                        document.querySelector('#downloadButtonNMP').innerHTML = this.strings.logs_valid_NMPbtn;
                                        document.getElementById('downloadButtonNMP').disabled = false;
                                    });
                                } else {
                                    Alertify.error(this.strings.logs_error_begin_date_superior);
                                }
                            } else {
                                Alertify.error(this.strings.logs_error_begin_date_inferior);
                            }
                        } else {
                            Alertify.error(this.strings.logs_error_empty_dates);
                        }
                    },

                    is_today(date) {
                      var today = new Date();
                      var dd= String(today.getDate()).padStart(2,'0');
                      var mm= String(today.getMonth() + 1).padStart(2,'0');
                      var yyyy = today.getFullYear();
                      today = yyyy+"-"+mm+"-"+dd;
                      var date = new Date(date);
                      if(date>=today) {
                          return true;
                      }
                      return false;
                    },

                    get_help_content() {
                        var help_contents = [];
                        var help = new Object();
                        help.title = this.strings.title;
                        help.description = this.strings.description;
                        help_contents.push(help);
                        return help_contents;
                    },

                    open_chart_help(chart) {
                        let contents = [];
                        if (chart == 'download_moodle') {
                            contents.push({
                                title: this.strings.logs_download_moodle_help_title,
                                description: this.strings.logs_download_moodle_help_description,
                            });
                        } else if (chart == "download_nmp") {
                            contents.push({
                                title: this.strings.logs_download_nmp_help_title,
                                description: this.strings.logs_download_nmp_help_description,
                            });
                        }
                        this.help_contents = contents;
                        if (this.help_contents.length) {
                            this.help_dialog = true;
                        }
                    },

                    update_help_dialog (value) {
                        this.help_dialog = value;
                    },

                    get_timezone(){
                        let information = `${this.strings.ss_change_timezone} ${this.timezone}`
                        return information;
                    },
                }
            })
        }

        return {
            init : init
        };
    });