define(["local_fliplearning/vue",
        "local_fliplearning/vuetify",
        "local_fliplearning/axios",
        "local_fliplearning/moment",
        "local_fliplearning/pagination",
        "local_fliplearning/chartstatic",
        "local_fliplearning/pageheader",
        "local_fliplearning/helpdialog",
    ],
    function(Vue, Vuetify, Axios, Moment, Pagination, ChartStatic, PageHeader, HelpDialog) {
        "use strict";

        function init(content) {
            // console.log(content);
            Vue.use(Vuetify);
            Vue.component('pagination', Pagination);
            Vue.component('chart', ChartStatic);
            Vue.component('pageheader', PageHeader);
            Vue.component('helpdialog', HelpDialog);
            let vue = new Vue({
                delimiters: ["[[", "]]"],
                el: "#work_sessions",
                vuetify: new Vuetify(),
                data() {
                    return {
                        strings : content.strings,
                        groups : content.groups,
                        userid : content.userid,
                        courseid : content.courseid,
                        timezone : content.timezone,
                        render_has : content.profile_render,
                        loading : false,
                        errors : [],

                        pages : content.pages,
                        hours_sessions: content.indicators.sessions,
                        session_count: content.indicators.count,
                        inverted_time: content.indicators.time,
                        inverted_time_colors: content.inverted_time_colors,
                        sessions_count_colors: content.sessions_count_colors,

                        search: null,

                        help_dialog: false,
                        help_contents: [],
                    }
                },
                mounted(){
                    document.querySelector("#sessions-loader").style.display = "none";
                    document.querySelector("#work_sessions").style.display = "block";
                    setTimeout(function() {
                        vue.setGraphicsEventListeners();
                    }, 500);
                },
                methods : {
                    get_help_content(){
                        let contents = [];
                        contents.push({
                            title: this.strings.section_help_title,
                            description: this.strings.section_help_description,
                        });
                        return contents;
                    },

                    update_interactions(week){
                        this.loading = true;
                        this.errors = [];
                        let data = {
                            action : "worksessions",
                            userid : this.userid,
                            courseid : this.courseid,
                            weekcode : week.weekcode,
                            profile : this.render_has,
                        }
                        Axios({
                            method:'get',
                            url: M.cfg.wwwroot + "/local/fliplearning/ajax.php",
                            params : data,
                        }).then((response) => {
                            if (response.status == 200 && response.data.ok) {
                                this.hours_sessions = response.data.data.indicators.sessions;
                                this.session_count = response.data.data.indicators.count;
                                this.inverted_time = response.data.data.indicators.time;
                            } else {
                                this.error_messages.push(this.strings.error_network);
                            }
                        }).catch((e) => {
                            this.errors.push(this.strings.api_error_network);
                        }).finally(() => {
                            this.loading = false;
                            //Ici, la page a fini de charger
                            vue.addLogsIntoDB("viewed", "week_"+week.weekcode, "week_section", "Week section that allows you to obtain information on a specific week");
                            vue.setGraphicsEventListeners();
                        });
                        return this.data;
                    },

                    get_point_category_name(point, dimension) {
                        let series = point.series,
                            isY = dimension === 'y',
                            axis = series[isY ? 'yAxis' : 'xAxis'];
                        return axis.categories[point[isY ? 'y' : 'x']];
                    },

                    build_hours_sessions_chart() {
                        let chart = new Object();
                        chart.title = {
                            text: null,
                        };
                        chart.chart = {
                            type: 'heatmap',
                            backgroundColor: null,
                            style: {fontFamily: 'poppins'},
                        };
                        chart.xAxis = {
                            categories: this.strings.days,
                        };
                        chart.yAxis = {
                            categories: this.strings.hours,
                            title: null,
                            reversed: true,
                        };
                        chart.colorAxis = {
                            min: 0,
                            minColor: '#E0E0E0',
                            maxColor: '#118AB2'
                        };
                        chart.legend = {
                            layout: 'horizontal',
                            verticalAlign: 'bottom',
                        };
                        chart.tooltip = {
                            formatter: function () {
                                let xCategoryName = vue.get_point_category_name(this.point, 'x');
                                let yCategoryName = vue.get_point_category_name(this.point, 'y');
                                let label = vue.strings.sessions_text;
                                if (this.point.value == 1) {
                                    label = vue.strings.session_text;
                                }
                                return '<b>' + xCategoryName + ' ' + yCategoryName + '</b>: '
                                    + this.point.value +' ' + label;
                            }
                        };
                        chart.series = [{
                            borderWidth: 2,
                            borderColor: '#FAFAFA',
                            data: this.hours_sessions,
                        }];
                        return chart;
                    },

                    build_inverted_time_chart() {
                        let chart = new Object();
                        chart.chart = {
                            type: 'bar',
                            backgroundColor: null,
                            style: {fontFamily: 'poppins'},
                        };
                        chart.title = {
                            text: null,
                        };
                        chart.colors = this.inverted_time_colors;
                        chart.xAxis = {
                            type: 'category',
                            crosshair: true,
                        };
                        chart.yAxis = {
                            title: {
                                text: this.strings.time_inverted_x_axis,
                            }
                        };
                        chart.tooltip = {
                            shared:true,
                            useHTML:true,
                            formatter: function () {
                                let category_name = this.points[0].key;
                                let time = vue.convert_time(this.y);
                                return `<b>${category_name}: </b>${time}`;
                            }
                        };
                        chart.legend = {
                            enabled: false
                        };
                        chart.series = [{
                            colorByPoint: true,
                            data: this.inverted_time.data
                        }];
                        return chart;
                    },

                    build_sessions_count_chart() {
                        let chart = new Object();
                        chart.chart = {
                            backgroundColor: null,
                            style: {fontFamily: 'poppins'},
                        };
                        chart.title = {
                            text: null,
                        };
                        chart.colors = this.sessions_count_colors;
                        chart.yAxis = {
                            title: {
                                text: this.strings.session_count_yaxis_title,
                            },
                            allowDecimals: false
                        };
                        chart.xAxis = {
                            categories: this.session_count.categories,
                        };
                        chart.tooltip = {
                            valueSuffix: this.strings.session_count_tooltip_suffix,
                        };
                        chart.legend = {
                            layout: 'horizontal',
                            verticalAlign: 'bottom',
                        };
                        chart.series = this.session_count.data
                        return chart;
                    },

                    convert_time(time) {
                        time *= 3600; // pasar las horas a segundos
                        let h = this.strings.hours_short;
                        let m = this.strings.minutes_short;
                        let s = this.strings.seconds_short;
                        let hours = Math.floor(time / 3600);
                        let minutes = Math.floor((time % 3600) / 60);
                        let seconds = Math.floor(time % 60);
                        let text;
                        if (hours >= 1) {
                            if (minutes >= 1) {
                                text = `${hours}${h} ${minutes}${m}`;
                            } else {
                                text = `${hours}${h}`;
                            }
                        } else if ((minutes >= 1)) {
                            if (seconds >= 1) {
                                text = `${minutes}${m} ${seconds}${s}`;
                            } else {
                                text = `${minutes}${m}`;
                            }
                        } else {
                            text = `${seconds}${s}`;
                        }
                        return text;
                    },

                    open_chart_help(chart) {
                        let contents = [];
                        var action = "";
                        var objectName = "";
                        var objectType = "";
                        var objectDescription = "";
                        if (chart == "inverted_time") {
                            contents.push({
                                title: this.strings.inverted_time_help_title,
                                description: this.strings.inverted_time_help_description_p1,
                            });
                            contents.push({
                                description: this.strings.inverted_time_help_description_p2,
                            });
                            action = "viewed";
                            objectType = "help";
                            objectName = "invested_time";
                            objectDescription = "Help section that provides information about the sessions per week chart";
                            vue.addLogsIntoDB(action, objectName, objectType, objectDescription);
                        } else if (chart == "hours_sessions") {
                            contents.push({
                                title: this.strings.hours_sessions_help_title,
                                description: this.strings.hours_sessions_help_description_p1,
                            });
                            contents.push({
                                description: this.strings.hours_sessions_help_description_p2,
                            });
                            action = "viewed";
                            objectType = "help";
                            objectName = "hours_sessions";
                            objectDescription = "Help section that provides information about the sessions per hour chart";
                            vue.addLogsIntoDB(action, objectName, objectType, objectDescription);
                        } else if (chart == "sessions_count") {
                            contents.push({
                                title: this.strings.sessions_count_help_title,
                                description: this.strings.sessions_count_help_description_p1,
                            });
                            contents.push({
                                description: this.strings.sessions_count_help_description_p2,
                            });
                            action = "viewed";
                            objectType = "help";
                            objectName = "sessions_count";
                            objectDescription = "Help section that provides information about the invested time chart";
                            vue.addLogsIntoDB(action, objectName, objectType, objectDescription);
                        }
                        this.help_contents = contents;
                        if (this.help_contents.length) {
                            this.help_dialog = true;
                        }
                    },

                    update_help_dialog (value) {
                        this.help_dialog = value;
                    },

                    get_timezone(){
                        let information = `${this.strings.ss_change_timezone} ${this.timezone}`
                        return information;
                    },

                    setGraphicsEventListeners() {
                        let graphics = document.querySelectorAll('.highcharts-container');
                        if(graphics.length<1) {
                            setTimeout(vue.setGraphicsEventListeners,500);
                        } else {
                            graphics[0].id="investedTime";
                            graphics[1].id="sessionsPerHour";
                            graphics[2].id="sessionsPerWeek";
                            graphics.forEach((graph) => {
                                graph.addEventListener('mouseenter', vue.addLogsViewGraphic);
                            })
                        }
                    },

                    addLogsViewGraphic(e) {
                        event.stopPropagation();
                        var action = "";
                        var objectName = "";
                        var objectType = "";
                        var objectDescription = "";
                        switch(e.target.id) {
                            case "investedTime":
                                action = "viewed";
                                objectName = "invested_time";
                                objectType = "chart";
                                objectDescription = "Bar chart that shows the average time invested by students as a function of the expected invested time";
                                break;
                            case "sessionsPerHour":
                                action = "viewed";
                                objectName = "hours_sessions";
                                objectType = "chart";
                                objectDescription = "Chart showing the number of sessions performed according to the time of day";
                                break;
                            case "sessionsPerWeek":
                                action = "viewed";
                                objectName = "sessions_count";
                                objectType = "chart";
                                objectDescription = "Chart showing the number of sessions performed per week";
                                break;
                            default:
                                action = "viewed";
                                objectName = "";
                                objectType = "chart";
                                objectDescription = "A chart";
                                break;
                        }
                        vue.addLogsIntoDB(action, objectName, objectType, objectDescription);
                    },

                    addLogsIntoDB(action, objectName, objectType, objectDescription) {
                        let data = {
                            courseid: content.courseid,
                            userid: content.userid,
                            action: "addLogs",
                            sectionname: "TEACHER_STUDY_SESSIONS",
                            actiontype: action,
                            objectType: objectType,
                            objectName: objectName,
                            currentUrl: document.location.href,
                            objectDescription: objectDescription,
                        };
                        Axios({
                            method:'get',
                            url: M.cfg.wwwroot + "/local/fliplearning/ajax.php",
                            params : data,
                        }).then((response) => {
                            if (response.status == 200 && response.data.ok) {
                            }
                        }).catch((e) => {
                        });
                    },
                }
            })

        }

        return {
            init : init
        };
    });