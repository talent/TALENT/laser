<?php

/**
 *
 * Plugin administration settings here
 *
 * @package     local_fliplearning
 * @author      2021 Éric Bart <bart.eric@hotmail.com>
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {
    $settings = new \admin_settingpage('local_notemyprogress', 'NoteMyProgress');
    $ADMIN->add('localplugins', $settings);

    // MongoDB hostname
    $label = get_string('fml_settings_bddaddress_label', 'local_fliplearning');
    $desc = get_string('fml_settings_bddaddress_description', 'local_fliplearning');
    $default = "localhost";
    $settings->add(new \admin_setting_configtext('local_notemyprogress/mongoDBlink', $label, $desc, $default));

    // MongoDB database port
    $label =  get_string('fml_settings_bddport_label', 'local_fliplearning');
    $desc = get_string('fml_settings_bddport_description', 'local_fliplearning');
    $default = "27017";
    $settings->add(new \admin_setting_configtext('local_notemyprogress/mongoDBport', $label, $desc, $default));

    // MongoDB Username
    $label = get_string('fml_settings_bddusername_label', 'local_fliplearning');
    $desc = get_string('fml_settings_bddusername_description', 'local_fliplearning');
    $default = "";
    $settings->add(new \admin_setting_configtext('local_notemyprogress/mongoDBusername', $label, $desc, $default));

    // MongoDB Password
    $label = get_string('fml_settings_bddpassword_label', 'local_fliplearning');
    $desc = get_string('fml_settings_bddpassword_description', 'local_fliplearning');
    $default = "";
    $settings->add(new \admin_setting_configtext('local_notemyprogress/mongoDBpassword', $label, $desc, $default));

    // MongoDB database name
    $label =  get_string('fml_settings_bddname_label', 'local_fliplearning');
    $desc =  get_string('fml_settings_bddname_description', 'local_fliplearning');
    $default = "";
    $settings->add(new \admin_setting_configtext('local_notemyprogress/mongoDBname', $label, $desc, $default));
}
