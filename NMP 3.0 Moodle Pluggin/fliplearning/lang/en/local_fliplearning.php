<?php
// Ce fichier fait partie de Moodle - http://moodle.org/
//
// Moodle est un logiciel libre: vous pouvez le redistribuer et / ou le modifier
// selon les termes de la licence publique générale GNU comme publié par
// la Free Software Foundation, soit la version 3 de la licence, soit
// (à votre choix) toute version ultérieure.
//
// Moodle est distribué dans l\'espoir qu'il sera utile,
// mais SANS AUCUNE GARANTIE; sans même la garantie implicite de
// QUALITÉ MARCHANDE ou d\'aDÉQUATION À UN USAGE PARTICULIER. Voir la
// Licence publique générale GNU pour plus de détails.
//
// Vous devriez avoir reçu une copie de la licence publique générale GNU
// avec Moodle. Sinon, consultez <http://www.gnu.org/licenses/>.

/**
* Plugin chains are defined here.
*
* @package local_fliplearning
* @category string
* @author 2021 Éric Bart <bart.eric@hotmail.com>
* @copyright 2020 Edisson Sigma <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
* @license http://www.gnu.org/copyleft /gpl.html GNU GPL v3 ou version ultérieure
*/

defined ('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'NoteMyProgress';

/* Global */
$string['pagination'] = 'Week:';
$string['graph_generating'] = 'We are building the report, please wait a moment.';
$string['weeks_not_config'] = 'The course has not been configured by the teacher, so there are no visualizations to display.';
$string['pagination_title'] = 'Selection of the week';
$string['helplabel'] = 'Help';
$string['exitbutton'] = 'OK!';
$string['no_data'] = 'There is no data to display';
$string['only_student'] = 'This report is for students only';
$string["fml_send_mail"] = "(Click to send an e-mail)";
$string["fml_about"] = "About this chart";
$string["fml_about_table"] = "About this table";
$string["fml_not_configured"] = "Not configured";
$string["fml_activated"] = "Activated";
$string["fml_disabled"] = "Disabled";

/* Menu */
$string['menu_main_title'] = "Progression of the scoreboard";
$string['menu_sessions'] = 'Study session';
$string['menu_setweek'] = "Define the weeks";
$string['menu_time'] = 'Time tracking';
$string['menu_assignments'] = 'Monitoring of deposits';
$string['menu_grades'] = 'Tracking grades';
$string['menu_quiz'] = 'Follow-up of evaluations';
$string['menu_dropout'] = 'Dropout';
$string['menu_logs'] = "Activity reports";
$string['menu_general'] = "Global indicators";

/* Nav Bar Menu */
$string['togglemenu'] = 'Show / Hide the NoteMyProgress menu';

/* Composant de pagination */
$string['pagination_component_to'] = 'al';
$string['pagination_component_name'] = 'Week';

/* Goups */
$string['group_allstudent'] = 'All students';

/* Erreurs générales */
$string['api_error_network'] = "An error occurred during communication with the server.";
$string['api_invalid_data'] = 'Invalid data';
$string['api_save_successful'] = 'The data has been correctly recorded on the server';
$string['api_cancel_action'] = 'You have cancelled the action ';

/* Admin Task Screen */
$string['generate_data_task'] = 'Data generation process for the NoteMyProgress plugin';

/* Graphique */
$string['chart_loading'] = 'Loading...';
$string['chart_exportButtonTitle'] = "Export";
$string['chart_printButtonTitle'] = "Print";
$string['chart_rangeSelectorFrom'] = "From";
$string['chart_rangeSelectorTo'] = "To";
$string['chart_rangeSelectorZoom'] = "Range";
$string['chart_downloadPNG'] = 'Download a PNG image';
$string['chart_downloadJPEG'] = 'Download a JPEG image';
$string['chart_downloadPDF'] = 'Download a PDF document';
$string['chart_downloadSVG'] = 'Download a SVG image';
$string['chart_downloadCSV'] = 'Download CSV';
$string['chart_downloadXLS'] = 'Download XLS';
$string['chart_exitFullscreen'] = 'Exit full screen';
$string['chart_hideData'] = 'Hide the data table';
$string['chart_noData'] = 'There is no data to display ';
$string['chart_printChart'] = 'Print the chart';
$string['chart_viewData'] = 'Display the data table';
$string['chart_viewFullscreen'] = 'View full screen';
$string['chart_resetZoom'] = 'Restart the zoom';
$string['chart_resetZoomTitle'] = 'Reset zoom level 1: 1';

/* Définir les semaines */
$string['setweeks_title'] = 'Course weeks configuration';
$string['setweeks_description'] = 'To start, you must set up the course by weeks and define a start date for the first week (the rest of the weeks will be done automatically from this date onwards). Then, you must associate the related activities or modules to each week by dragging them from the right column to the corresponding week.  It is not necessary to assign all the activities or modules to the weeks, just those that you want to consider for tracking the students. Finally, click on the Save button to keep your settings. ';
$string['setweeks_sections'] = "Sections available in the course";
$string['setweeks_weeks_of_course'] = "Planning the weeks";
$string['setweeks_add_new_week'] = "Add a week";
$string['setweeks_start'] = "Start the:";
$string['setweeks_end'] = "End the:";
$string['setweeks_week'] = "Week";
$string['setweeks_save'] = "Save configuration";
$string['setweeks_time_dedication'] = 'How many hours of work do you expect students to put into your course this week ?';
$string['setweeks_enable_scroll'] = "Activate the scroll mode for weeks and themes";
$string['setweeks_label_section_removed'] = "Removed from the course";
$string['setweeks_error_section_removed'] = "A section assigned to a week has been removed from the course, you must remove it from your plan to continue.";
$string['setweeks_save_warning_title'] = "Are you sure you want to save the changes ?";
$string['setweeks_save_warning_content'] = "If you change the configuration of weeks where the course has already started, data may be lost...";
$string['setweeks_confirm_ok'] = "Save";
$string['setweeks_confirm_cancel'] = "Cancel";
$string['setweeks_error_empty_week'] = "You cannot save changes with an empty week. Please delete it and try again.";
$string['setweeks_new_group_title'] = "New instance of configuration";
$string['setweeks_new_group_text'] = "We have detected that your course is finished, if you wish to set up the weeks to work with new students you need to activate the button below. This will separate the data of current students from previous courses, avoiding mixing them up. ";
$string['setweeks_new_group_button_label'] = "Save the configuration as a new instance";
$string['course_format_weeks'] = 'Week';
$string['course_format_topics'] = 'Topic';
$string['course_format_social'] = 'Social';
$string['course_format_singleactivity'] = 'Single activity';
$string['plugin_requirements_title'] = 'Status:';
$string['plugin_requirements_descriptions'] = 'The plugin will be visible and display reports for students and teachers when the following conditions are met...';
$string['plugin_requirements_has_users'] = 'The course must have at least one student enrolled';
$string['plugin_requirements_course_start'] = 'The current date must be later than the start date of the first configured week.';
$string['plugin_requirements_has_sections'] = 'The configured weeks must contain at least one section.';
$string['plugin_visible'] = 'Visible reports.';
$string['plugin_hidden'] = 'Hidden reports.';
$string['title_conditions'] = 'Terms of use ';

/* Heure */
$string['fml_mon'] = 'Monday';
$string['fml_tue'] = 'Tuesday';
$string['fml_wed'] = 'Wednesday';
$string['fml_thu'] = 'Thursday';
$string['fml_fri'] = 'Friday';
$string['fml_sat'] = 'Saturday';
$string['fml_sun'] = 'Sunday';
$string['fml_mon_short'] = 'Mon';
$string['fml_tue_short'] = 'Tue';
$string['fml_wed_short'] = 'Wed';
$string['fml_thu_short'] = 'Thu';
$string['fml_fri_short'] = 'Fri';
$string['fml_sat_short'] = 'Sat';
$string['fml_sun_short'] = 'Sun';

$string['fml_jan'] = 'January';
$string['fml_feb'] = 'February';
$string['fml_mar'] = 'March';
$string['fml_apr'] = 'April';
$string['fml_may'] = 'May';
$string['fml_jun'] = 'June';
$string['fml_jul'] = 'July';
$string['fml_aug'] = 'August';
$string['fml_sep'] = 'September';
$string['fml_oct'] = 'October';
$string['fml_nov'] = 'November';
$string['fml_dec'] = 'December';
$string['fml_jan_short'] = 'Jan';
$string['fml_feb_short'] = 'Feb';
$string['fml_mar_short'] = 'Mar';
$string['fml_apr_short'] = 'Apr';
$string['fml_may_short'] = 'May';
$string['fml_jun_short'] = 'Jun';
$string['fml_jul_short'] = 'Jul';
$string['fml_aug_short'] = 'Aug';
$string['fml_sep_short'] = 'Sep';
$string['fml_oct_short'] = 'Oct';
$string['fml_nov_short'] = 'Nov';
$string['fml_dec_short'] = 'Dec';

$string['fml_week1'] = 'Week 1';
$string['fml_week2'] = 'Week 2';
$string['fml_week3'] = 'Week 3';
$string['fml_week4'] = 'Week 4';
$string['fml_week5'] = 'Week 5';
$string['fml_week6'] = 'Week 6';

$string['fml_00'] = '12:00am';
$string['fml_01'] = '01:00am';
$string['fml_02'] = '02:00am';
$string['fml_03'] = '03:00am';
$string['fml_04'] = '04:00am';
$string['fml_05'] = '05:00am';
$string['fml_06'] = '06:00am';
$string['fml_07'] = '07:00am';
$string['fml_08'] = '08:00am';
$string['fml_09'] = '09:00am';
$string['fml_10'] = '10:00am';
$string['fml_11'] = '11:00am';
$string['fml_12'] = '12:00pm';
$string['fml_13'] = '01:00pm';
$string['fml_14'] = '02:00pm';
$string['fml_15'] = '03:00pm';
$string['fml_16'] = '04:00pm';
$string['fml_17'] = '05:00pm';
$string['fml_18'] = '06:00pm';
$string['fml_19'] = '07:00pm';
$string['fml_20'] = '08:00pm';
$string['fml_21'] = '09:00pm';
$string['fml_22'] = '10:00pm';
$string['fml_23'] = '11:00pm';

/* Enseignant général */
$string['tg_section_help_title'] = 'Global indicators';
$string['tg_section_help_description'] = 'This section contains views with general indicators related to course setup, resources assigned per week, study sessions and student progress through the course. The views in this section show indicators from the start date to the end date of the course (or the current date if the course is not yet finished).';
$string['tg_week_resources_help_title'] = 'Resources per week';
$string['tg_week_resources_help_description_p1'] = 'This graph displays the amount of resources for each of the course sections assigned to each study week configured in the <i>Configure Weeks</i> section. If a week has two or more course sections assigned to it, the resources of those sections are added together for the calculation of the total resources for a week. ';
$string['tg_week_resources_help_description_p2'] = 'On the x-axis of the graph are the total resources and activities of the sections assigned to each configured week of NoteMyProgress. On the y-axis are the configured study weeks. ';
$string['tg_weeks_sessions_help_title'] = 'Sessions per week';
$string['tg_week_sessions_help_description_p1'] = 'This graph shows the number of study sessions completed by students in each week from the start date of the course. The student\'s access to the course is considered as the start of a study session. A session is considered completed when the time elapsed between two student interactions exceeds 30 minutes. ';
$string['tg_week_sessions_help_description_p2'] = 'On the x-axis of the graph are the weeks of each month. The y-axis of the graph shows the different months of the year from the month of course creation. To maintain the symmetry of the graph, a total of five weeks has been placed for each month, however, each month does not have that many weeks. These months will only add sessions up to the fourth week.';
$string['tg_progress_table_help_title'] = 'Student progress';
$string['tg_progress_table_help_description'] = 'This table shows a list of all students enrolled in the course together with their progress, number of sessions and time spent. For the calculation of the progress all the resources of the course have been considered except for those of type <i>Label</i>. To determine whether a student has completed a resource, the first step is to check whether the resource has the completeness setting enabled. If so, we look to see if the student has already completed the activity based on that setting. Otherwise, the activity is considered complete if the student has viewed it at least once. ';

$string['fml_title'] = 'Work sessions';
$string['table_title'] = 'Course progress';
$string['thead_name'] = 'First name';
$string['thead_lastname'] = 'Last name';
$string['thead_email'] = 'Mail';
$string['thead_progress'] = 'Progression (%)';
$string['thead_sessions'] = 'Sessions';
$string['thead_time'] = 'Time invested';

$string['fml_module_label'] = 'ressource';
$string['fml_modules_label'] = 'ressources';
$string['fml_of_conector'] = 'of';
$string['fml_finished_label'] = 'finished';
$string['fml_finisheds_label'] = 'finished';

$string['fml_smaller30'] = 'Under 30 minutes';
$string['fml_greater30'] = 'Over 30 minutes';
$string['fml_greater60'] = 'Over 60 minutes';

$string['fml_session_count_title'] = 'Sessions of the week';
$string['fml_session_count_yaxis_title'] = 'Number of Sessions';
$string['fml_session_count_tooltip_suffix'] = ' sessions';

$string['fml_hours_sessions_title'] = 'Sessions by day and time';
$string['fml_weeks_sessions_title'] = 'Sessions per week';

$string["fml_session_text"] = "session";
$string["fml_sessions_text"] = "sessions";

$string['ss_change_timezone'] = 'Timezone:';
// $string['ss_activity_inside_plataform_student'] = 'Mon activité sur la plateforme';
// $string['ss_activity_inside_plataform_teacher'] = 'Activité des étudiants sur la plateforme';
// $string['ss_time_inside_plataform_student'] = 'Mon temps sur la plateforme';
// $string['ss_time_inside_plataform_teacher'] = 'Temps moyen passé par les étudiants sur la plateforme cette semaine';
// $string['ss_time_inside_plataform_description_teacher'] = 'Temps que l’élève a investi dans la semaine sélectionnée, par rapport au temps que l’enseignant a prévu de l’investir. Le temps passé affiché correspond à la moyenne de tous les élèves. Le temps prévu par l’enseignant est le temps attribué par l’enseignant dans <i> Configurer les semaines </i>. ';
// $string['ss_time_inside_plataform_description_student'] = 'Temps passé cette semaine par rapport au temps que l’enseignant a prévu de passer.';
// $string['ss_activity_inside_plataform_description_teacher'] = 'Les heures de la journée sont indiquées sur l\'axe Y et les jours de la semaine sur l\'axe X. Dans le graphique, vous pouvez trouver plusieurs points qui, en les survolant, offrent des informations détaillées sur les interactions des étudiants, regroupées par type de ressource (nombre d\'interactions, nombre d\'étudiants qui ont interagi avec la ressource et moyenne des interactions). <br/> <br/> <b> En cliquant sur les balises, vous pourrez filtrer par type de ressource, ne laissant visibles que celles qui ne sont pas barrées. </b> ';
// $string['ss_activity_inside_plataform_description_student'] = 'Afficher les interactions par type de ressource et planification. Lorsque vous survolez un point visible du graphique, vous verrez le nombre d\'interactions regroupées par type de ressource. En cliquant sur les balises, vous pourrez filtrer par type de ressource. ';

/* Sessions de l\'enseignant */
$string['ts_section_help_title'] = 'Study sessions';
$string['ts_section_help_description'] = 'This section contains visualisations related to student activity in the course measured in terms of sessions completed, average time spent in the course per week and study sessions at time intervals. The data presented in this section varies depending on the study week selected. ';
$string['ts_inverted_time_help_title'] = 'Time invested by students';
$string['ts_inverted_time_help_description_p1'] = 'This graph shows the average time spent by students during the week compared to the average time expected by the teacher.';
$string['ts_inverted_time_help_description_p2'] = 'On the x-axis of the graph is the number of hours the teacher has planned for a specific week. On the y-axis are the labels for average time spent and average time that should be spent. ';
$string['ts_hours_sessions_help_title'] = 'Sessions by day and time';
$string['ts_hours_sessions_help_description_p1'] = 'This graph shows the study sessions by day and time of the selected week. The student\'s access to the course is considered as the start of a study session. A session is considered finished when the time elapsed between two student interactions exceeds 30 minutes. ';
$string['ts_hours_sessions_help_description_p2'] = 'On the x-axis of the graph are the days of the week. On the y-axis are the hours of the day starting at 12am and ending at 11pm or 11pm.';
$string['ts_sessions_count_help_title'] = 'Sessions of the Week';
$string['ts_sessions_count_help_description_p1'] = 'This graph shows the number of sessions classified by their duration in time ranges: less than 30 minutes, more than 30 minutes and more than 60 minutes. The student\'s access to the course is considered as the start of a study session. A session is considered finished when the time elapsed between two student interactions exceeds 30 minutes.';
$string['ts_sessions_count_help_description_p2'] = 'On the x-axis of the graph are the days of the configured week. On the y-axis is the number of sessions held.';

$string['fml_time_inverted_title'] = 'Time invested by students';
$string['fml_time_inverted_x_axis'] = 'Number of hours';
$string['fml_inverted_time'] = 'Average time invested';
$string['fml_expected_time'] = 'Average time to invest';

$string['fml_year'] = 'year';
$string['fml_years'] = 'years';
$string['fml_month'] = 'month';
$string['fml_months'] = 'months';
$string['fml_day'] = 'day';
$string['fml_days'] = 'days';
$string['fml_hour'] = 'hour';
$string['fml_hours'] = 'hours';
$string['fml_hours_short'] = 'h';
$string['fml_minute'] = 'minute';
$string['fml_minutes'] = 'minutes';
$string['fml_minutes_short'] = 'm';
$string['fml_second'] = 'second';
$string['fml_seconds'] = 'seconds';
$string['fml_seconds_short'] = 's';
$string['fml_ago'] = 'ago';
$string['fml_now'] = 'now';

/*Devoirs des enseignants */

$string['ta_section_help_title'] = 'Task monitoring';
$string['ta_section_help_description'] = 'This section contains indicators related to the delivery of assignments to drop-off areas and access to resources. The data presented in this section will vary depending on the study week selected. ';
$string['ta_assigns_submissions_help_title'] = 'Follow-up of homework submitted in the deposit areas';
$string['ta_assigns_submissions_help_description_p1'] = 'This graph shows the distribution of the number of students, in relation to the delivery status of an assignment in the drop-off areas.';
$string['ta_assigns_submissions_help_description_p2'] = 'On the x-axis of the graph are the names of the repositories for the selected week\'s sections with the date and time of expected delivery. On the y-axis is the distribution of the number of students according to the delivery status submitted (green), not submitted (red), submitted late (yellow). By clicking on the different areas of the graph, you can send an email to the groups of students you want (those who sent the assignment late or not, those who sent their assignment on time...).';
$string['ta_access_content_help_title'] = 'Access to the course content ';
$string['ta_access_content_help_description_p1'] = 'This graph shows the number of students who have and have not accessed the course resources. At the top are the different types of Moodle resources, with the ability to filter the graph information according to the type of resource selected. ';
$string['ta_access_content_help_description_p2'] = 'The x-axis of the graph shows the number of students enrolled in the course. The y-axis of the graph shows the section resources assigned for the week. In addition, this graph allows you to send an email to students who have accessed the resource or to those who have not by clicking on the graph. ';

/* Assign Submissions */
$string['fml_intime_sub'] = 'Assignment submitted on time';
$string['fml_late_sub'] = 'Late submission of assignments.';
$string['fml_no_sub'] = 'Assignment not submitted';
$string['fml_assign_nodue'] = 'No deadline';
$string['fml_assignsubs_title'] = 'Follow-up of submitted assignments in the deposit areas';
$string['fml_assignsubs_yaxis'] = 'Number of students';


/* Accès au contenu */
$string['fml_assign'] = 'Assign';
$string['fml_assignment'] = 'Assignment';
$string['fml_attendance'] = 'Attendance';
$string['fml_book'] = 'Book';
$string['fml_chat'] = 'Chat';
$string['fml_choice'] = 'Choice';
$string['fml_data'] = 'Database';
$string['fml_feedback'] = 'Feedback';
$string['fml_folder'] = 'Folder';
$string['fml_forum'] = 'Forum';
$string['fml_glossary'] = 'Glossary';
$string['fml_h5pactivity'] = 'H5P';
$string['fml_imscp'] = 'IMS Content';
$string['fml_label'] = 'Label';
$string['fml_lesson'] = 'Lesson';
$string['fml_lti'] = 'LTI Content';
$string['fml_page'] = 'Page';
$string['fml_quiz'] = 'Quiz';
$string['fml_resource'] = 'Resource';
$string['fml_scorm'] = 'Package SCORM';
$string['fml_survey'] = 'Survey';
$string['fml_url'] = 'Url';
$string['fml_wiki'] = 'Wiki';
$string['fml_workshop'] = 'Workshop';

$string['fml_access'] = 'Accessed';
$string['fml_no_access'] = 'No access';
$string['fml_access_chart_title'] = 'Access to the course content';
$string['fml_access_chart_yaxis_label'] = 'Number of students';
$string['fml_access_chart_suffix'] = 'students';


/* Email */
$string['fml_validation_subject_text'] = 'The subject is mandatory';
$string['fml_validation_message_text'] = 'Please write a message';
$string['fml_subject_label'] = 'Add a topic';
$string['fml_message_label'] = 'Add a message';

$string['fml_submit_button'] = 'Send';
$string['fml_cancel_button'] = 'Cancel';
$string['fml_close_button'] = 'Close';
$string['fml_emailform_title'] = 'Send an e-mail';
$string['fml_sending_text'] = 'Sending emails';

$string['fml_recipients_label'] = 'To';
$string['fml_mailsended_text'] = 'mails sent';

$string['fml_email_footer_text'] = 'This is an email sent with note my progress.';
$string['fml_email_footer_prefix'] = 'Go to';
$string['fml_email_footer_suffix'] = 'for more information.';
$string['fml_mailsended_text'] = 'Emails sent';

$string['fml_assign_url'] = '/mod/assign/view.php?id=';
$string['fml_assignment_url'] = '/mod/assignment/view.php?id=';
$string['fml_book_url'] = '/mod/book/view.php?id=';
$string['fml_chat_url'] = '/mod/chat/view.php?id=';
$string['fml_choice_url'] = '/mod/choice/view.php?id=';
$string['fml_data_url'] = '/mod/data/view.php?id=';
$string['fml_feedback_url'] = '/mod/feedback/view.php?id=';
$string['fml_folder_url'] = '/mod/folder/view.php?id=';
$string['fml_forum_url'] = '/mod/forum/view.php?id=';
$string['fml_glossary_url'] = '/mod/glossary/view.php?id=';
$string['fml_h5pactivity_url'] = '/mod/h5pactivity/view.php?id=';
$string['fml_imscp_url'] = '/mod/imscp/view.php?id=';
$string['fml_label_url'] = '/mod/label/view.php?id=';
$string['fml_lesson_url'] = '/mod/lesson/view.php?id=';
$string['fml_lti_url'] = '/mod/lti/view.php?id=';
$string['fml_page_url'] = '/mod/page/view.php?id=';
$string['fml_quiz_url'] = '/mod/quiz/view.php?id=';
$string['fml_resource_url'] = '/mod/resource/view.php?id=';
$string['fml_scorm_url'] = '/mod/scorm/view.php?id=';
$string['fml_survey_url'] = '/mod/survey/view.php?id=';
$string['fml_url_url'] = '/mod/url/view.php?id=';
$string['fml_wiki_url'] = '/mod/wiki/view.php?id=';
$string['fml_workshop_url'] = '/mod/workshop/view.php?id=';
$string['fml_course_url'] = '/course/view.php?id=';


/* Évaluation de l\'enseignant */
$string['tr_section_help_title'] = 'Suivi des notes';
$string['tr_section_help_description'] = 'This section contains indicators related to grade averages in assessable activities. The different teaching units (Qualification Categories) created by the teacher are displayed in the <i>Qualification Category </i> selector. This selector will allow you to switch between the different units defined and show the activities that can be assessed in each. ';
$string['tr_grade_items_average_help_title'] = 'Average of assessable activities';
$string['tr_grade_items_average_help_description_p1'] = 'This graph shows the average (percentage) score of students in each of the assessable activities in the course. The percentage average is calculated based on the maximum score for the assessable activity (e.g. an assessable activity with a maximum score of 80 and an average score of 26 will have a bar height of 33%, as 26 is 33% of the total score). The grade point average has been expressed in terms of percentages in order to preserve the symmetry of the graph, as Moodle allows you to create activities and assign custom grades. ';
$string['tr_grade_items_average_help_description_p2'] = 'On the x-axis of the graph are the different assessable activities of the course. On the y-axis is the weighted average expressed as a percentage. ';
$string['tr_grade_items_average_help_description_p3'] = 'Clicking on the bar for an assessable activity will update the data in the two lower graphs to show additional information about the selected assessable activity.';
$string['tr_item_grades_details_help_title'] = 'Best grade, worst grade and average grade';
$string['tr_item_grades_details_help_description_p1'] = 'This graph shows the best, average and worst score for an assessable activity (the activity selected in the table of average assessable activities).';
$string['tr_item_grades_details_help_description_p2'] = 'On the x-axis of the graph is the activity score, with the maximum activity score being the maximum value on this axis. On the y-axis are the labels for the best score, average score and worst score. ';
$string['tr_item_grades_distribution_help_title'] = 'Distribution of grades';
$string['tr_item_grades_distribution_help_description_p1'] = 'This graph shows the distribution of students in different grade ranges. The score ranges are calculated according to percentages. The following ranges are considered: less than 50%, more than 50%, more than 60%, more than 70%, more than 80% and more than 90%. These ranges are calculated according to the maximum weight that the teacher assigns to an assessable activity. ';
$string['tr_item_grades_distribution_help_description_p2'] = 'On the x-axis are the ranges of activity scores. On the y-axis is the number of students belonging to a certain rank. ';
$string['tr_item_grades_distribution_help_description_p3'] = 'By clicking on the bar corresponding to a rank, you can send an email to the students in the ranking.';

/* Notes */
$string['fml_grades_select_label'] = 'Grade category';
$string['fml_grades_chart_title'] = 'Averages of assessable activities';
$string['fml_grades_yaxis_title'] = 'Average grade (%)';
$string['fml_grades_tooltip_average'] = 'Average grade';
$string['fml_grades_tooltip_grade'] = 'Highest grade';
$string['fml_grades_tooltip_student'] = 'student graded from';
$string['fml_grades_tooltip_students'] = 'students graded from';

$string['fml_grades_best_grade'] = 'Highest grades';
$string['fml_grades_average_grade'] = 'Average grade';
$string['fml_grades_worst_grade'] = 'Worst grade';
$string['fml_grades_details_subtitle'] = 'Best grade, worst grade and average grade';

$string['fml_grades_distribution_subtitle'] = 'Distribution of grades';
$string['fml_grades_distribution_greater_than'] = 'greater than';
$string['fml_grades_distribution_smaller_than'] = 'lower than';
$string['fml_grades_distribution_yaxis_title'] = 'Number of students';
$string['fml_grades_distribution_tooltip_prefix'] = 'Range';
$string['fml_grades_distribution_tooltip_suffix'] = 'in this range';
$string["fml_view_details"] = "(Click to see details)";


/* Quiz enseignant */
$string['tq_section_help_title'] = 'Follow-up of evaluations';
$string['tq_section_help_description'] = 'This section contains indicators related to the summary of attempts in the various course evaluations and the analysis of evaluation questions. The data presented in this section varies according to the week of study selected and a selector containing all the Evaluation type activities in the course sections assigned to the selected week.';
$string['tq_questions_attempts_help_title'] = 'Attempted questions';
$string['tq_questions_attempts_help_description_p1'] = 'This graph shows the distribution of resolution attempts for each question in an assessment and their revision status.';
$string['tq_questions_attempts_help_description_p2'] = 'On the x-axis of the graph are the assessment questions. The y-axis shows the number of attempts to solve each of these questions. The symmetry of the graph will be affected by the assessment parameters (e.g. in an assessment that always has the same questions, the graph will show the same number of attempts for each bar corresponding to a question. In an assessment with random questions (from a question bank), the graph will show in the bar for each question the sum of the assessment attempts in which it appeared, and may not be the same for each assessment question). ';
$string['tq_questions_attempts_help_description_p3'] = 'By clicking on one of the bars corresponding to a question, it is possible to see the assessment question in a pop-up window.';
$string['tq_hardest_questions_help_title'] = 'More difficult questions';
$string['tq_hardest_questions_help_description_p1'] = 'This graph shows the assessment questions sorted by difficulty level. An attempt to solve a question with the status Partially correct, incorrect or blank is considered incorrect, so the total number of incorrect attempts for a question is the sum of attempts with the above statuses. The level of difficulty is represented as a percentage calculated on the basis of the total number of attempts. ';
$string['tq_hardest_questions_help_description_p2'] = 'On the x-axis of the graph are the assessment questions identified by name. The y-axis shows the percentage of incorrect attempts relative to the total number of attempts for the question. This axis helps to identify the questions that represented the greatest difficulty for the students who took the assessment. ';
$string['tq_hardest_questions_help_description_p3'] = 'By clicking on one of the bars corresponding to a question, it is possible to see the assessment question in a pop-up window.';

$string["fml_quiz_info_text"] = "This evaluation has";
$string["fml_question_text"] = "question";
$string["fml_questions_text"] = "questions";
$string["fml_doing_text_singular"] = "attempt made by";
$string["fml_doing_text_plural"] = "attempts made by";
$string["fml_attempt_text"] = "attempt";
$string["fml_attempts_text"] = "attempts";
$string["fml_student_text"] = "student";
$string["fml_students_text"] = "students";
$string["fml_quiz"] = "Assessments";
$string["fml_questions_attempts_chart_title"] = "Attempted questions";
$string["fml_questions_attempts_yaxis_title"] = "No. of attempts";
$string["fml_hardest_questions_chart_title"] = "More difficult questions";
$string["fml_hardest_questions_yaxis_title"] = "Incorrect attempts";
$string["fml_correct_attempt"] = "Correct";
$string["fml_partcorrect_attempt"] = "Partially correct";
$string["fml_incorrect_attempt"] = "Wrong";
$string["fml_blank_attempt"] = "Empty";
$string["fml_needgraded_attempt"] = "Not rated";
$string["fml_review_question"] = "(Click to review the question)";


/* Abandon */
$string['td_section_help_title'] = 'Dropout';
$string['td_section_help_description'] = 'This section contains indicators related to the prediction of student dropout in a course. The information is displayed in terms of student groups calculated by an algorithm that analyses the behaviour of each student in terms of the time invested, the number of student sessions, the number of days of activity and the interactions they have had with each resource and with other students in the course. The algorithm places students with similar behaviour in the same group, so that students who are increasingly unengaged in the course can be identified. The data presented in this section varies depending on the group selected in the selector that contains the groups identified in the course. ';
$string['td_group_students_help_title'] = 'Grouping students together';
$string['td_group_students_help_description_p1'] = 'This table shows the students who belong to the group selected in the student group selector. Each student\'s photo, names and percentage of progress in the course are listed. For the purposes of calculating progress, all the resources in the course have been taken into account, with the exception of those of the Label type. To determine if a student has completed a resource, it is first checked to see if the resource completeness setting is enabled. If so, it is checked to see if the student has already completed the activity based on that setting. Otherwise, the activity is considered complete if the student has seen it at least once. ';
$string['td_group_students_help_description_p2'] = 'Clicking on a student in this table will update the charts below with the selected student\'s information.';
$string['td_modules_access_help_title'] = 'Course resources';
$string['td_modules_access_help_description_p1'] = 'This graph shows the amount of resources the student has accessed and completed. The data presented in this graph varies depending on the student selected in the Student Group table. To determine the amount of resources and complete activities, the Moodle setting called Complete Activities is used. If the teacher does not set up the completeness of course activities, the number of activities accessed and completed will always be the same, as without such a set up, a resource is considered completed when the student accesses it. ';
$string['td_modules_access_help_description_p2'] = 'The x-axis shows the number of resources in the course. On the y-axis are the labels of the accessed, complete and total resources of the course. ';
$string['td_modules_access_help_description_p3'] = 'By clicking on any bar it is possible to see the resources and activities available in the course (in a pop-up window) as well as the number of student interactions with each resource and a label of not accessed, accessed or completed. ';
$string['td_week_modules_help_title'] = 'Resources per week';
$string['td_week_modules_help_description_p1'] = 'This graph shows the amount of resources accessed and completed by the student for each week configured in the plugin. The data presented in this graph varies depending on the student selected in the <i>Student Group</i> table. ';
$string['td_week_modules_help_description_p2'] = 'On the x-axis of the graph are the different study weeks configured. The y-axis shows the amount of resources and activities accessed and completed by the student. ';
$string['td_week_modules_help_description_p3'] = 'By clicking on any bar it is possible to see the resources and activities available in the course (in a pop-up window) as well as the number of student interactions with each resource and a label of not accessed, accessed or completed. ';
$string['td_sessions_evolution_help_title'] = 'Sessions and time spent';
$string['td_sessions_evolution_help_description_p1'] = 'This graph shows how study sessions have evolved since your first session was recorded in the course. The data presented in this graph varies depending on the student selected in the <i>Student Group</i> table. ';
$string['td_sessions_evolution_help_description_p2'] = 'The x-axis of the graph shows a timeline with the days that have elapsed since the student did the first study session until the day of the last recorded session. On the y-axis, they display 2 values, on the left side the number of student sessions and on the right side the time spent in hours. Between these axes, the number of sessions and the time spent by the student are drawn as a time series. ';
$string['td_sessions_evolution_help_description_p3'] = 'This visualisation allows you to zoom in on a selected region. This approach makes it possible to clearly show this development in different date ranges. ';
$string['td_user_grades_help_title'] = 'Grades';
$string['td_user_grades_help_description_p1'] = 'This graph shows a comparison of the student\'s grades with the grade averages (mean percentage) of their peers in the various assessable activities of the course. The data presented in this graph varies depending on the student selected in the <i>Student Group</i> table. ';
$string['td_user_grades_help_description_p2'] = 'The various assessable activities are displayed on the x-axis of the graph. On the y-axis are the student\'s grade and the average grade of their peers. The student\'s grade and the course average are displayed as a percentage to maintain the symmetry of the graph. ';
$string['td_user_grades_help_description_p3'] = 'With a click on the bar corresponding to an activity, it is possible to go to the analysed activity. ';

$string["fml_cluster_label"] = "Group";
$string["fml_cluster_select"] = 'Student group';
$string["fml_dropout_table_title"] = "Students in the group";
$string["fml_dropout_see_profile"] = "View profile";
$string["fml_dropout_user_never_access"] = "Never accessed";
$string["fml_dropout_student_progress_title"] = "Student's progression";
$string["fml_dropout_student_grade_title"] = "Grade";
$string['fml_dropout_no_data'] = "There is no dropout data for this course yet";
$string['fml_dropout_no_users_cluster'] = "There are no students in this group";
$string['fml_dropout_generate_data_manually'] = "Generate manually";
$string['fml_dropout_generating_data'] = "Generating data...";
$string["fml_modules_access_chart_title"] = "Course resources";
$string["fml_modules_access_chart_series_total"] = "Total";
$string["fml_modules_access_chart_series_complete"] = "Completed";
$string["fml_modules_access_chart_series_viewed"] = "Accessed";
$string["fml_week_modules_chart_title"] = "Resources per week";
$string["fml_modules_amount"] = "Quantity of resources";
$string["fml_modules_details"] = "(Click to view resources)";
$string["fml_modules_interaction"] = "interaction";
$string["fml_modules_interactions"] = "interactions";
$string["fml_modules_viewed"] = "Accessed";
$string["fml_modules_no_viewed"] = "Not accessed";
$string["fml_modules_complete"] = "Completed";
$string["fml_sessions_evolution_chart_title"] = "Sessions and time invested";
$string["fml_sessions_evolution_chart_xaxis1"] = "No. of sessions";
$string["fml_sessions_evolution_chart_xaxis2"] = "No. of hours";
$string["fml_sessions_evolution_chart_legend1"] = "No. of sessions";
$string["fml_sessions_evolution_chart_legend2"] = "Time invested";
$string["fml_user_grades_chart_title"] = "Grades";
$string["fml_user_grades_chart_yaxis"] = "Percentage grade";
$string["fml_user_grades_chart_xaxis"] = "Assessable activities";
$string["fml_user_grades_chart_legend"] = "Course (average)";
$string["fml_user_grades_chart_tooltip_no_graded"] = "No rating";
$string["fml_user_grades_chart_view_activity"] = "Click to view the activity";
$string['fml_send_mail_to_user'] = 'Send an e-mail to';
$string['fml_send_mail_to_group'] = 'Send an e-mail to the group';


/* Général étudiant */
$string['sg_section_help_title'] = 'General indicators';
$string['sg_section_help_description'] = 'This section contains indicators related to your information, progress, general indicators, course resources, sessions throughout the course and grades. The displays in this section show indicators throughout the course (up to the current date). ';
$string['sg_modules_access_help_title'] = 'Course resources';
$string['sg_modules_access_help_description_p1'] = 'This graph shows the amount of resources you have accessed and completed. To determine how many resources you have completed, use the Moodle setting called Activity Completion. If the teacher has not set up the completeness of the course activities, the number of activities accessed and completed will always be the same, as without such a setting a resource is considered completed when you access it. ';
$string['sg_modules_access_help_description_p2'] = 'On the x-axis is the amount of resources in the course. On the y-axis are the labels of the accessible, complete and total resources in reference to your interactions with the course resources. ';
$string['sg_modules_access_help_description_p3'] = 'By clicking on any bar, it is possible to see the resources and activities available in the course (in a pop-up window) as well as the number of interactions you have had with each resource and a label of not accessed, accessed or completed. ';
$string['sg_weeks_session_help_title'] = 'Sessions per week';
$string['sg_weeks_session_help_description_p1'] = 'This graph shows the number of study sessions you have completed each week from the start date of the course. A session is considered completed when the time between two interactions exceeds 30 minutes. ';
$string['sg_weeks_session_help_description_p2'] = 'On the x-axis of the graph are the weeks of each month. The y-axis of the graph shows the different months of the year from the month of course creation. To maintain the symmetry of the graph, a total of five weeks has been placed for each month, however, each month does not have that many weeks. These months will only add sessions up to the fourth week. ';
$string['sg_sessions_evolution_help_title'] = 'Sessions and time invested';
$string['sg_sessions_evolution_help_description_p1'] = 'This graph shows how your study sessions have evolved since your first session was enrolled in the course. ';
$string['sg_sessions_evolution_help_description_p2'] = 'The x-axis of the graph shows a timeline with the days that have passed since your first study session to the day of your last recorded session. On the y-axis, they display 2 values, on the left side your number of sessions and on the right side your time spent in hours. Between these axes, your number of sessions and your time spent as a student are represented as a time series. ';
$string['sg_sessions_evolution_help_description_p3'] = 'This view allows you to zoom in on a selected region.';
$string['sg_user_grades_help_title'] = 'Grades';
$string['sg_user_grades_help_description_p1'] = 'This graph shows a comparison of your grades with the average grades (percentage average) of your classmates in the different assessable activities of the course.';
$string['sg_user_grades_help_description_p2'] = 'The x-axis of the graph shows the different activities that can be assessed. On the y-axis you will find your grades and the average grade of your classmates. Your grade and the course average are displayed as percentages to maintain the symmetry of the graph. ';
$string['sg_user_grades_help_description_p3'] = 'By clicking on the bar corresponding to an activity, it is possible to access the one analysed. ';

/* Sessions utilisateur */
$string['ss_section_help_title'] = 'Study sessions';
$string['ss_section_help_description'] = 'This section contains visualisations with indicators related to your activity in the course measured in terms of study sessions, time spent and progress in each of the weeks configured by the teacher. The displays in this section vary depending on the study week selected. ';
$string['ss_inverted_time_help_title'] = 'Your time invested';
$string['ss_inverted_time_help_description_p1'] = 'This graph shows your time spent in the week compared to the time planned by the teacher.';
$string['ss_inverted_time_help_description_p2'] = 'On the x-axis of the graph is the number of hours the teacher has scheduled for a specific week. On the y-axis are the labels for time spent and time to be spent. ';
$string['ss_hours_session_help_title'] = 'Sessions by day and time';
$string['ss_hours_session_help_description_p1'] = 'This graph shows your study sessions by day and time of the selected week. A session is considered completed when the time between two interactions exceeds 30 minutes. ';
$string['ss_hours_session_help_description_p2'] = 'On the x-axis of the graph are the days of the week. On the y-axis are the hours of the day starting at 12:00am and ending at 11:00am. ';
$string['ss_resources_access_help_title'] = 'Interaction by type of resource';
$string['ss_resources_access_help_description_p1'] = 'This graph shows how many resources you have pending and which ones you have already completed in the selected week. Resources are grouped by type in this graph. In addition, a bar is displayed at the top which represents the percentage of resources accessed in relation to the total resources assigned to the selected week. ';
$string['ss_resources_access_help_description_p2'] = 'The x-axis of the graph shows the different types of resources. The y-axis shows the amount of resources accessed for the week.';
$string['ss_resources_access_help_description_p3'] = 'By clicking on any bar, it is possible to see the resources and activities available in the course (in a pop-up window) as well as the number of interactions you have had with each resource and a label of not accessed, accessed or completed. ';


$string['fml_student_time_inverted_title'] = 'Your time invested';
$string['fml_student_time_inverted_x_axis'] = 'No. of hours';
$string['fml_student_inverted_time'] = 'Time invested';
$string['fml_student_expected_time'] = 'Time to invest';

$string['fml_resource_access_title'] = 'Interaction by type of resource';
$string['fml_resource_access_y_axis'] = 'Quantity of resources';
$string['fml_resource_access_x_axis'] = 'Types of resources';
$string['fml_resource_access_legend1'] = 'Completed';
$string['fml_resource_access_legend2'] = 'Waiting';

$string['fml_week_progress_title'] = 'Progress of the week';



/* Indicateurs de l\'enseignant */
$string['fml_teacher_indicators_title'] = 'General indicators';
$string['fml_teacher_indicators_students'] = 'Students';
$string['fml_teacher_indicators_weeks'] = 'Weeks';
$string['fml_teacher_indicators_grademax'] = 'Grades';
$string['fml_teacher_indicators_course_start'] = 'Start the';
$string['fml_teacher_indicators_course_end'] = 'End the';
$string['fml_teacher_indicators_course_format'] = 'Format';
$string['fml_teacher_indicators_course_completion'] = 'Completeness of the modules';
$string["fml_teacher_indicators_student_progress"] = "Student progress";
$string["fml_teacher_indicators_week_resources_chart_title"] = "Resources per week";
$string["fml_teacher_indicators_week_resources_yaxis_title"] = "Quantity of resources";

/* Logs section */
$string['fml_logs_title'] = 'Download the activity logs';
$string['fml_logs_help_description'] = 'This section allows you to download the activity logs that have been performed. That is, you have access to the actions that have been carried out by registered users on the platform in a spreadsheet format.';
$string['fml_logs_title_MoodleSetpoint_title'] = 'Select a date range for actions on Moodle';
$string['fml_logs_title_MMPSetpoint_title'] = 'Select a date range for actions performed on Note My Progress';
$string['fml_logs_help'] = 'This section allows you to download a log file of activities performed.';
$string['fml_logs_select_date'] = 'Select a time interval for the log';
$string['fml_logs_first_date'] = 'Start date';
$string['fml_logs_last_date'] = 'End date';
$string['fml_logs_valid_Moodlebtn'] = 'Download the Moodle activity log';
$string['fml_logs_valid_NMPbtn'] = 'Download the Note My Progress activity log';
$string['fml_logs_invalid_date'] = 'Please enter a date';
$string['fml_logs_download_btn'] = 'Download in progress';
$string['fml_logs_download_nmp_help_title'] = 'About the actions carried out on Note My Progress';
$string['fml_logs_download_moodle_help_title'] = 'About the actions carried out on Moodle';
$string['fml_logs_download_nmp_help_description'] = 'The log file that is downloaded lists all the actions that have been performed by the user within the Note My Progress plugin only (viewing progress, viewing general indicators, etc.).';
$string['fml_logs_download_moodle_help_description'] = 'The log file that is uploaded lists all the actions that have been performed by the user within Moodle only (viewing the course, viewing resources, submitting an assignment, etc).';




/* Logfiles + Logs section */
$string['fml_logs_csv_headers_username'] = 'Username';
$string['fml_logs_csv_headers_firstname'] = 'First name';
$string['fml_logs_csv_headers_lastname'] = 'Last name';
$string['fml_logs_csv_headers_date'] = 'Date';
$string['fml_logs_csv_headers_hour'] = 'Hour';
$string['fml_logs_csv_headers_action'] = 'Action';
$string['fml_logs_csv_headers_coursename'] = 'Course name';
$string['fml_logs_csv_headers_detail'] = 'Object_Name';
$string['fml_logs_csv_headers_detailtype'] = 'Object_Type';

$string['fml_logs_error_begin_date_superior'] = 'The start date cannot be later than the current date';
$string['fml_logs_error_begin_date_inferior'] = 'The start date must be earlier than the end date';
$string['fml_logs_error_empty_dates'] = 'Dates cannot be empty';
$string['fml_logs_error_problem_encountered'] = 'A problem has been encountered, please try again';

$string['fml_logs_success_file_downloaded'] = 'File uploaded!';

$string['fml_logs_moodle_csv_headers_role'] = 'Role';
$string['fml_logs_moodle_csv_headers_email'] = 'Email';
$string['fml_logs_moodle_csv_headers_username'] = 'Username';
$string['fml_logs_moodle_csv_headers_fullname'] = 'Fullname';
$string['fml_logs_moodle_csv_headers_date'] = 'Date';
$string['fml_logs_moodle_csv_headers_hour'] = 'Hour';
$string['fml_logs_moodle_csv_headers_action'] = 'Action';
$string['fml_logs_moodle_csv_headers_courseid'] = 'CourseID';
$string['fml_logs_moodle_csv_headers_coursename'] = 'Course_name';
$string['fml_logs_moodle_csv_headers_detailid'] = 'Detail ID';
$string['fml_logs_moodle_csv_headers_details'] = 'Details';
$string['fml_logs_moodle_csv_headers_detailstype'] = 'Details_type';

$string['fml_logs_moodle_csv_headers_role_description'] = 'Gives the role the user has on the course on which they have taken an action (student, teacher...)';
$string['fml_logs_moodle_csv_headers_email_description'] = 'Gives the user\'s e-mail address';
$string['fml_logs_moodle_csv_headers_username_description'] = 'Gives the moodle username of the person who performed the action';
$string['fml_logs_moodle_csv_headers_fullname_description'] = 'Gives the user\'s full name (First + Last)';
$string['fml_logs_moodle_csv_headers_date_description'] = 'Gives the date on which the action was performed in the format dd-MM-YYYY';
$string['fml_logs_moodle_csv_headers_hour_description'] = 'Gives the time at which the action was performed';
$string['fml_logs_moodle_csv_headers_action_description'] = 'Give a verb describing the action that has been performed (e.g. clicked, viewed...)';
$string['fml_logs_moodle_csv_headers_courseid_description'] = 'Gives the ID on which the action was performed';
$string['fml_logs_moodle_csv_headers_coursename_description'] = 'Give the name of the price on which the action was carried out';
$string['fml_logs_moodle_csv_headers_detailid_description'] = 'Gives the ID of the object with which the user has interacted';
$string['fml_logs_moodle_csv_headers_details_description'] = 'Give the name of the object that was targeted';
$string['fml_logs_moodle_csv_headers_detailstype_description'] = 'Gives the type of object that has been targeted (examples of objects: Repository, Quiz, Resources...)';

$string['fml_logs_nmp_csv_headers_role_description'] = 'Gives the role the user has on the course on which they have taken an action (student, teacher...)';
$string['fml_logs_nmp_csv_headers_email_description'] = 'Gives the user\'s e-mail address';
$string['fml_logs_nmp_csv_headers_username_description'] = 'Gives the moodle username of the person who performed the action';
$string['fml_logs_nmp_csv_headers_fullname_description'] = 'Gives the user\'s full name (First + Last)';
$string['fml_logs_nmp_csv_headers_date_description'] = 'Gives the date on which the action was performed in the format dd-MM-YYYY';
$string['fml_logs_nmp_csv_headers_hour_description'] = 'Gives the time at which the action was performed';
$string['fml_logs_nmp_csv_headers_courseid_description'] = 'Gives the price identifier on which the action was performed';
$string['fml_logs_nmp_csv_headers_section_name_description'] = 'Gives the name of the section of note my progress that the user was in when they performed the action';
$string['fml_logs_nmp_csv_headers_action_type_description'] = 'Gives a full description of the action that was performed by the user in the form of verb + subject + object (e.g. downloaded_moodle_logfile)';

$string['fml_logs_moodle_table_title'] = 'Description of headings';
$string['fml_logs_moodle_table_subtitle'] = 'Regarding Moodle logs';

$string['fml_logs_nmp_table_title'] = 'Description of headings';
$string['fml_logs_nmp_table_subtitle'] = 'Regarding Note My Progress logs';

$string['fml_logs_nmp_csv_headers_role'] = 'Role';
$string['fml_logs_nmp_csv_headers_email'] = 'Email';
$string['fml_logs_nmp_csv_headers_username'] = 'Username';
$string['fml_logs_nmp_csv_headers_fullname'] = 'Fullname';
$string['fml_logs_nmp_csv_headers_date'] = 'Date';
$string['fml_logs_nmp_csv_headers_hour'] = 'Hour';
$string['fml_logs_nmp_csv_headers_courseid'] = 'CourseID';
$string['fml_logs_nmp_csv_headers_section_name'] = 'NMP_SECTION_NAME';
$string['fml_logs_nmp_csv_headers_action_type'] = 'NMP_ACTION_TYPE';

$string['fml_logs_table_title'] = 'Heading';
$string['fml_logs_table_title_bis'] = 'Description';

$string['fml_logs_help_button_nmp'] = 'About the actions carried out on Note My Progress';
$string['fml_logs_help_button_moodle'] = 'About the actions carried out on Moodle';

$string['fml_logs_download_details_link'] = 'Read more';
$string['fml_logs_download_details_title'] = 'Are you sure you want a detailed explanation report?';
$string['fml_logs_download_details_description'] = 'If you accept, a file in PDF format will be downloaded.';
$string['fml_logs_download_details_ok'] = 'Download';
$string['fml_logs_download_details_cancel'] = 'Cancel';
$string['fml_logs_download_details_validation'] = 'The report has been downloaded';

/* NoteMyProgress admin settings */

$string['fml_settings_bddusername_label'] = 'Database username';
$string['fml_settings_bddusername_description'] = 'This parameter designates the username from which the MongoDB database can be accessed. If this parameter is entered, you will need to enter the password and the name of the database you wish to connect to.';
$string['fml_settings_bddusername_default'] = 'Empty';

$string['fml_settings_bddpassword_label'] = 'Account password';
$string['fml_settings_bddpassword_description'] = 'This parameter is the password for the account from which the MongoDB database can be accessed. If this parameter is entered, you will need to enter the username and the name of the database you wish to connect to.';
$string['fml_settings_bddpassword_default'] = 'Empty';


$string['fml_settings_bddaddress_label'] = 'MongoDB server address *';
$string['fml_settings_bddaddress_description'] = 'This parameter is the address from which the MongoDB database is accessible. This parameter is mandatory and is in the form: 151.125.45.58 or yourserver.com';
$string['fml_settings_bddaddress_default'] = 'localhost';

$string['fml_settings_bddport_label'] = 'Communication port *';
$string['fml_settings_bddport_description'] = 'This parameter designates the port to be used to communicate with the database. This parameter is mandatory and must be a number.';
$string['fml_settings_bddport_default'] = '27017';


$string['fml_settings_bddname_label'] = 'Name of the database';
$string['fml_settings_bddname_description'] = 'This parameter designates the name of the MongoDB database in which the information will be stored.';
$string['fml_settings_bddname_default'] = 'Empty';



