<?php


/**
 * Plugin logs functions are defined here.
 *
 * @package     local_fliplearning
 * @author      2021 Éric Bart <bart.eric@hotmail.com>
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace local_fliplearning;

require_once dirname(__FILE__) . '/../../../course/lib.php';
require 'server/vendor/autoload.php';
defined('MOODLE_INTERNAL') || die();

use MongoDB\Client as MongoDBLink;
use context_course;

class logs {

    public $beginDate;
    public $lastDate;
    public $courseid;
    public $userid;
    public $client;

    public function __construct($courseid, $userid) {
        $this->courseid=$courseid;
        $this->userid=$userid;
        self::connectmongoDB();
    }

    public function connectmongoDB() {
        global $DB;
        $connexionInfo = $DB->get_records("config_plugins", array("plugin" => "local_notemyprogress"), "name, value");
        foreach($connexionInfo as $co) {
            if($co->name == "mongoDBlink") {
                $dbLink = $co;
            }
            if($co->name == "mongoDBname") {
                $dbName = $co;
            }
            if($co->name === "mongoDBpassword") {
                $dbPassword = $co;
            }
            if($co->name == "mongoDBport") {
                $dbPort = $co;
            }
            if($co->name == "mongoDBusername") {
                $dbUsername = $co;
            }
        }
        if($dbLink->value=="") {
            $dbLink->value = "localhost";
        }
        if($dbPort->value=="") {
            $dbPort->value = "27017";
        }
	$dbPortSize = str_split($dbPort->value);
	if(count($dbPortSize)<=5) {
		if($dbUsername->value=="") {
			$this->client = new MongoDBLink("mongodb://$dbLink->value:$dbPort->value");
		} else if(!empty($dbUsername->value)){
			$this->client = new MongoDBLink("mongodb://$dbUsername->value:$dbPassword->value@$dbLink->value:$dbPort->value/?authSource=$dbName->value");
		}
	}
    }

    public function addLogsNMP($actionType, $objectType, $sectionName, $objectName, $currentLink, $objectDescription = null) {
        $context = context_course::instance($this->courseid);
        $roles = get_user_roles($context, $this->userid);
        $rolename = "";
        foreach ($roles as $role) {
            $rolename = $role->shortname;
        }

        if(is_siteadmin($this->userid)) {
            $rolename = "administrator";
        }

        $userInformations=self::getUserIdentification($this->userid);
        $nmpDB = $this->client->nmpDB;
        $nmpDBCollection = $nmpDB->logsNMP;
        $nmpDBCollection->insertOne(['actor' =>
                                        ['objectType'=>$rolename,
                                         'mbox'=>$userInformations['email'],
                                         'name'=>$userInformations['firstname'].' '.$userInformations['lastname'],
                                         'account' =>
                                            ['name'=>$userInformations['username']]
                                     ],
                                     'verb' =>
                                         ['id'=>'https://www.irit.fr/laser/nmp/xapi/verbs/'.$actionType,
                                             'display'=>
                                                 ['en-US'=>$actionType]
                                         ],
                                         'object' =>
                                             ['id'=>'https://www.irit.fr/laser/nmp/xapi/object/'.$objectType,
                                              'objectType'=>$objectType,
                                              'definition' => [
                                                  'name'=>
                                                      ['en-US'=>$objectName],
                                                  'description'=>
                                                      ['en-US'=>$objectDescription]
                                              ],
                                              'context'=>
                                                  [ 'contextActivities'=>
                                                      ['grouping'=>$sectionName]
                                                  ]
                                              ]
                                                ,
                                     'context'=>
                                         ['platform' => 'Moodle',
                                              'contextActivities'=>
                                                  ['grouping'=>
                                                      ['id'=>$currentLink]
                                                  ]
                                         ],
                                     'timestamp'=>time()
                ]
        );
    }

    public function searchLogsNMP($beginDate, $lastDate)
    {
        $nmpDB = $this->client->nmpDB;
        $nmpDBCollection = $nmpDB->logsNMP;
        $this->beginDate=$beginDate;
        $this->lastDate=$lastDate;
        $lastDate = strtotime("+1 day", strtotime($lastDate));
        $beginDate = strtotime($beginDate);
        $find = $nmpDBCollection->find(
            ['timestamp' => ['$gt'=>$beginDate,'$lt'=>$lastDate]]
        );
        return self::generateLogsNMP($find);
    }

    public function generateLogsNMP($data) {
        self::remove_old_logs($this->courseid);
        $filename = "ActivityLogsNMP_Course". $this->courseid . ".csv";
        $path = dirname(__FILE__) . "/../downloads/";
        $csv = fopen($path.$filename, "w+");
        $entetes = array(self::accent_remover(get_string("fml_logs_nmp_csv_headers_role", "local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_nmp_csv_headers_email","local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_nmp_csv_headers_username","local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_nmp_csv_headers_fullname","local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_nmp_csv_headers_date","local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_nmp_csv_headers_hour","local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_nmp_csv_headers_courseid","local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_nmp_csv_headers_section_name", "local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_nmp_csv_headers_action_type", "local_fliplearning")),
                        );
        fputcsv($csv, $entetes, ";");
        foreach ($data as $row) {
            $row->object->definition->name->{'en-US'} = self::accent_remover($row->object->definition->name->{'en-US'});
            if(str_contains($row->object->definition->name->{'en-US'}, '-') || str_contains($row->object->definition->name->{'en-US'}, ' ')) {
                $row->object->definition->name->{'en-US'} = str_replace(array(' ', '-'), array('_','_'), $row->object->definition->name->{'en-US'});
            }
            $rolename = $row->actor->objectType;
            $url = parse_url($row->context->contextActivities->grouping->id);
            $url = parse_str($url['query'], $params);
            $tabData = array($rolename,
                            $row->actor->mbox,
                            $row->actor->account->name,
                            $row->actor->name,
                            date('d-m-y', $row->timestamp),
                            date("H:i:s", $row->timestamp),
                            $params['courseid'],
                            $row->object->context->contextActivities->grouping,
                            $row->verb->display->{'en-US'} . '-' . $row->object->definition->name->{'en-US'} . '-' . $row->object->objectType,
            );
            fputcsv($csv, $tabData, ";");
        }
        fclose($csv);
        return $filename;
    }

    /**
     * Cette fonction a pour but d'aller chercher dans la base de donnée toutes les logs qui sont contenues entre
     * les deux dates qui sont spécifiées par l'utilisateur ($beginDate & $lastDate)
     *
     * @param $beginDate  Date minimum de la recherche
     * @param $lastDate   Date maximum de la recherche
     * @return mixed      Tous les logs contenus entre cet interval
     */
    public function searchLogsMoodle($beginDate, $lastDate)
    {
        global $DB;
        $this->beginDate=$beginDate;
        $this->lastDate=$lastDate;
        $lastDate = strtotime("+1 day", strtotime($lastDate));
        $beginDate = strtotime($beginDate);
        $sql = "SELECT * from {logstore_standard_log} WHERE timecreated>={$beginDate} AND timecreated<={$lastDate} AND courseid={$this->courseid}";
        $find = $DB->get_records_sql($sql);
        return self::generateLogsMoodle($find);
    }

    /**
     * Génère un fichier .csv contenant toutes les logs d'une date donnée
     *
     * @param array $data Tableau contenant les données
     */
    public function generateLogsMoodle($data)
    {
        self::remove_old_logs($this->courseid);
        $context = context_course::instance($this->courseid);
        $roles = get_user_roles($context, $this->userid);
        $rolename = "";
        foreach ($roles as $role) {
            $rolename = $role->shortname;
        }
        if(is_siteadmin($this->userid)) {
            $rolename = "administrator";
        }
        $filename = "ActivityLogsMoodle_Course". $this->courseid . ".csv";
        $path = dirname(__FILE__) . "/../downloads/";
        $csv = fopen($path.$filename, "w+");
        $entetes = array(self::accent_remover(get_string("fml_logs_moodle_csv_headers_role", "local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_moodle_csv_headers_email", "local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_moodle_csv_headers_username", "local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_moodle_csv_headers_fullname", "local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_moodle_csv_headers_date", "local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_moodle_csv_headers_hour", "local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_moodle_csv_headers_action", "local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_moodle_csv_headers_courseid", "local_fliplearning")),
                        self::accent_remover(get_string("fml_logs_moodle_csv_headers_coursename", "local_fliplearning")),
                        "OBJECT_ID",
                        "OBJECT_NAME",
                        "OBJECT_TYPE");
        fputcsv($csv, $entetes, ";");
        foreach ($data as $res => $val) {
            $name = self::getUserIdentification($val->userid);
            $course = self::getCourse($val->courseid);
            $detail = self::getDetail($val->objectid, $val->objecttable);
            //$val->timecreated += 3600; //3600 correspond à 1h de notre temps, on ajoute 1h à l'heure de création car l'heure de création a un décalage de -1h.
            $tabData = array(
                $rolename,
                $name['email'],
                $name['username'],
                $name['firstname'].' '.$name['lastname'],
                date("d-m-y", $val->timecreated),
                date("H:i:s", $val->timecreated),
                $val->action,
                $val->courseid,
                $course,
                $val->objectid,
                $detail,
                $val->objecttable);
            fputcsv($csv, $tabData, ";");
        }
        fclose($csv);
        return $filename;
    }

    /**
     * Retourne un tableau associatif contenant l'username, le nom ainsi que le prénom de la personne visée
     *
     * @param $userid Identifiant de l'utilisateur
     * @return array  Tableau associatif contenant username, firstname & lastname de l'utilisateur
     */
    public function getUserIdentification($userid)
    {
        global $DB;
        $name = (array)$DB->get_record("user", array("id" => $userid), "id, username, lastname, firstname, email"); //On cherche le nom de la personne qui a fait la log
        if ($name) {
            if (empty($name['username'])) {
                $name['username'] = "Undefined";
            }
            if (empty($name['lastname'])) {
                $name['lastname'] = "Undefined";
            }
            if (empty($name['firstname'])) {
                $name['firstname'] = "Undefined";
            }
            if (empty($name['email'])) {
                $name['email'] = "Undefined";
            }
            return $name;
        }
        $name['username'] = "Not found";
        $name['username'] = "Not found";
        $name['firstname'] = "Not found";
        return $name;
    }

    /**
     * Retourne un tableau associatif contenant le nom complet du cours visé
     *
     * @param $courseid Identifiant du cours
     * @return array    Tableau associatif contenant le nom complet du cours
     */
    public function getCourse($courseid)
    {
        global $DB;
        $course = (array)$DB->get_record("course", array("id" => $courseid), "fullname");
        $course = self::accent_remover($course['fullname']);
        if ($course) {
            return $course;
        }
        $course = "";
        return $course;
    }

    /**
     * Fonction permettant de récupérer les informations précises des actions des utilisateurs
     *
     * @param $objectid id de l'objet visé (test, dépôt, etc..)
     * @param $datatable base de données où aller chercher l'information
     * @return string|string[] informations retournées
     */
    public function getDetail($objectid, $datatable)
    {
        global $DB;
        if (!empty($objectid) && !empty($datatable)) {
            if ($datatable == 'assign' || $datatable == 'assignment' || $datatable == 'book'
                || $datatable == 'chat' || $datatable == 'choice' || $datatable == 'data' || $datatable == 'forum'
                || $datatable == 'glossary' || $datatable == 'imscp' || $datatable == 'lesson' || $datatable == 'label'
                || $datatable == 'lti' || $datatable == 'page' || $datatable == 'quiz' || $datatable == 'resource'
                || $datatable == 'scorm' || $datatable == 'url' || $datatable == 'wiki' || $datatable == 'workshop'
                || $datatable == 'folder' || $datatable == 'course_sections' || $datatable == "enrol") {
                $detail = (array)$DB->get_record($datatable, array("id" => $objectid), "name", "id");
                $detail = self::accent_remover($detail['name']);
            } else if ($datatable == 'grade_items') {
                $detail = (array)$DB->get_record($datatable, array("id" => $objectid), "itemname", "id");
                $detail = self::accent_remover($detail['itemname']);
            }
            return $detail;
        }
    }

    /**
     * Fonction permettant d'harmoniser le fichier csv avec un encodage sans accent, permet d'éviter d'eventuels
     * bugs de rendu
     *
     * @param $cadena chaîne à vérifier
     * @return string|string[] chaîne $cadena sans accent
     */
    public function accent_remover($cadena)
    {
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );
        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena);
        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena);
        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena);
        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena);
        $cadena = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $cadena
        );
        return $cadena;
    }

    /**
     * Enlève l'ancien fichier de logs généré ayant le même identifiant de cours dans un souci d'éviter les
     * duplications dans le disque du serveur.
     *
     * @param $courseid l'identifiant du cours contenu dans le nom du fichier à supprimer
     *
     */
    public static function remove_old_logs($courseid){
        $path = dirname(__FILE__) . "/../downloads";
        $files = glob($path . '/*');
        foreach($files as $file){
            if(is_file($file)){
                $route_parts = explode(".", $file);
                foreach($route_parts as $route_part){
                    if($route_part == $courseid){
                        unlink($file);
                    }
                }
            }
        }
    }

    /**
     * Fonction permettant d'enlever les identifiants inutiles
     * /!\ Retirée car finalement inutile dans le traitement du CSV.
     * @param $value valeur à vérifier
     * @return string valeur vide si id inutile
     */
    //public function clearCSV($value)
    //{
    //    if ($value == 0 || $value == "0") {
    //        $value = "";
    //    }
    //    return $value;
    //}

    /**
     * Fonction permettant de télécharger le fichier de logs directement grace à du code php (utile pour debug)
     */
    //public function downloadFile()
    //{
    //    $file = "Logs_" . $_GET['file'];
    //    $tempDir = sys_get_temp_dir();
    //    header("Cache-Control: private");
    //    header("Content-Description: File Transfer");
    //    header("Content-Disposition: attachment; filename=$file.csv");
    //    header("Content-Type: application/csv");
    //    header("Content-Transfer-Emcoding: binary");
    //    readfile("$tempDir" . DIRECTORY_SEPARATOR . "data.csv");
    //}
}
?>
