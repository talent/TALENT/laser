<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local fliplearning
 *
 * @package     local_fliplearning
 * @author      2021 Éric Bart <bart.eric@hotmail.com>
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('locallib.php');
global $COURSE, $USER;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);


$url = '/local/fliplearning/logs.php';
local_fliplearning_set_page($course, $url);

require_capability('local/fliplearning:usepluggin', $context);
require_capability('local/fliplearning:view_as_teacher', $context);
require_capability('local/fliplearning:logs', $context);

$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$logs = new \local_fliplearning\logs($COURSE->id, $USER->id);
$logs->addLogsNMP("viewed", "section", "LOGFILES", "activity_log", $actualLink, "Section where you can consult the logs of the activities performed on the course and on the note my progress plugin");

$reports = new \local_fliplearning\teacher($COURSE->id, $USER->id);

$configweeks = new \local_fliplearning\configweeks($COURSE, $USER);
if (!$configweeks->is_set()) {
    $message = get_string("weeks_not_config", "local_fliplearning");
    print_error($message);
}

$content = [
    "courseid" => $course->id,
    "userid" => $USER->id,
    'timezone' => $reports->timezone,
    'strings' => [
        "section_help_title" => get_string("fml_logs_help_title", "local_fliplearning"),
        "section_help_description" => get_string("fml_logs_help", "local_fliplearning"),

        "title" => get_string("fml_logs_title", "local_fliplearning"),
        "description" => get_string("fml_logs_help_description", "local_fliplearning"),
        "helplabel" => get_string("helplabel","local_fliplearning"),
        "exitbutton" => get_string("exitbutton","local_fliplearning"),
        "ss_change_timezone" => get_string("ss_change_timezone", "local_fliplearning"),
        "graph_generating" => get_string("graph_generating", "local_fliplearning"),
        "about" => get_string("fml_about", "local_fliplearning"),
        "about_table" => get_string("fml_about_table", "local_fliplearning"),

        "logs_indicators_first_date" => get_string("fml_logs_first_date", "local_fliplearning"),
        "logs_indicators_last_date" => get_string("fml_logs_last_date", "local_fliplearning"),
        "logs_indicators_title_select" => get_string("fml_logs_select_date", "local_fliplearning"),
        "logs_valid_date" => get_string("fml_logs_valid_date", "local_fliplearning"),
        "logs_valid_Moodlebtn" => get_string("fml_logs_valid_Moodlebtn", "local_fliplearning"),
        "logs_valid_NMPbtn" => get_string("fml_logs_valid_NMPbtn", "local_fliplearning"),
        "logs_invalid_date" => get_string("fml_logs_invalid_date", "local_fliplearning"),
        "logs_title_MoodleSetpoint_title" => get_string("fml_logs_title_MoodleSetpoint_title", "local_fliplearning"),
        "logs_title_MMPSetpoint_title" => get_string("fml_logs_title_MMPSetpoint_title", "local_fliplearning"),
        "logs_download_btn" => get_string("fml_logs_download_btn", "local_fliplearning"),

        "logs_download_nmp_help_title" =>  get_string("fml_logs_download_nmp_help_title", "local_fliplearning"),
        "logs_download_moodle_help_title" =>  get_string("fml_logs_download_moodle_help_title", "local_fliplearning"),
        "logs_download_moodle_help_description" => get_string("fml_logs_download_moodle_help_description","local_fliplearning"),
        "logs_download_nmp_help_description" => get_string("fml_logs_download_nmp_help_description","local_fliplearning"),

        "logs_moodle_csv_headers_role" => get_string("fml_logs_moodle_csv_headers_role", "local_fliplearning"),
        "logs_moodle_csv_headers_email" => get_string("fml_logs_moodle_csv_headers_email","local_fliplearning"),
        "logs_moodle_csv_headers_username" => get_string("fml_logs_moodle_csv_headers_username","local_fliplearning"),
        "logs_moodle_csv_headers_fullname" =>  get_string("fml_logs_moodle_csv_headers_fullname","local_fliplearning"),
        "logs_moodle_csv_headers_date" => get_string("fml_logs_moodle_csv_headers_date","local_fliplearning"),
        "logs_moodle_csv_headers_hour" => get_string("fml_logs_moodle_csv_headers_hour","local_fliplearning"),
        "logs_moodle_csv_headers_action" => get_string("fml_logs_moodle_csv_headers_action","local_fliplearning"),
        "logs_moodle_csv_headers_courseid" => get_string("fml_logs_moodle_csv_headers_courseid","local_fliplearning"),
        "logs_moodle_csv_headers_coursename" => get_string("fml_logs_moodle_csv_headers_coursename","local_fliplearning"),
        "logs_moodle_csv_headers_detailid" => get_string("fml_logs_moodle_csv_headers_detailid", "local_fliplearning"),
        "logs_moodle_csv_headers_details" => get_string("fml_logs_moodle_csv_headers_details", "local_fliplearning"),
        "logs_moodle_csv_headers_detailstype" => get_string("fml_logs_moodle_csv_headers_detailstype", "local_fliplearning"),

        "logs_moodle_csv_headers_role_description" => get_string("fml_logs_moodle_csv_headers_role_description", "local_fliplearning"),
        "logs_moodle_csv_headers_email_description" => get_string("fml_logs_moodle_csv_headers_email_description","local_fliplearning"),
        "logs_moodle_csv_headers_username_description" => get_string("fml_logs_moodle_csv_headers_username_description","local_fliplearning"),
        "logs_moodle_csv_headers_fullname_description" =>  get_string("fml_logs_moodle_csv_headers_fullname_description","local_fliplearning"),
        "logs_moodle_csv_headers_date_description" => get_string("fml_logs_moodle_csv_headers_date_description","local_fliplearning"),
        "logs_moodle_csv_headers_hour_description" => get_string("fml_logs_moodle_csv_headers_hour_description","local_fliplearning"),
        "logs_moodle_csv_headers_action_description" => get_string("fml_logs_moodle_csv_headers_action_description","local_fliplearning"),
        "logs_moodle_csv_headers_courseid_description" => get_string("fml_logs_moodle_csv_headers_courseid_description","local_fliplearning"),
        "logs_moodle_csv_headers_coursename_description" => get_string("fml_logs_moodle_csv_headers_coursename_description","local_fliplearning"),
        "logs_moodle_csv_headers_detailid_description" => get_string("fml_logs_moodle_csv_headers_detailid_description", "local_fliplearning"),
        "logs_moodle_csv_headers_details_description" => get_string("fml_logs_moodle_csv_headers_details_description", "local_fliplearning"),
        "logs_moodle_csv_headers_detailstype_description" => get_string("fml_logs_moodle_csv_headers_detailstype_description", "local_fliplearning"),



        "logs_nmp_csv_headers_role" => get_string("fml_logs_nmp_csv_headers_role", "local_fliplearning"),
        "logs_nmp_csv_headers_email" => get_string("fml_logs_nmp_csv_headers_email","local_fliplearning"),
        "logs_nmp_csv_headers_username" => get_string("fml_logs_nmp_csv_headers_username","local_fliplearning"),
        "logs_nmp_csv_headers_fullname" =>  get_string("fml_logs_nmp_csv_headers_fullname","local_fliplearning"),
        "logs_nmp_csv_headers_date" => get_string("fml_logs_nmp_csv_headers_date","local_fliplearning"),
        "logs_nmp_csv_headers_hour" => get_string("fml_logs_nmp_csv_headers_hour","local_fliplearning"),
        "logs_nmp_csv_headers_courseid" => get_string("fml_logs_nmp_csv_headers_courseid","local_fliplearning"),
        "logs_nmp_csv_headers_section_name" => get_string("fml_logs_nmp_csv_headers_section_name", "local_fliplearning"),
        "logs_nmp_csv_headers_action_type" => get_string("fml_logs_nmp_csv_headers_action_type", "local_fliplearning"),

        "logs_nmp_csv_headers_role_description" => get_string("fml_logs_nmp_csv_headers_role_description", "local_fliplearning"),
        "logs_nmp_csv_headers_email_description" => get_string("fml_logs_nmp_csv_headers_email_description","local_fliplearning"),
        "logs_nmp_csv_headers_username_description" => get_string("fml_logs_nmp_csv_headers_username_description","local_fliplearning"),
        "logs_nmp_csv_headers_fullname_description" =>  get_string("fml_logs_nmp_csv_headers_fullname_description","local_fliplearning"),
        "logs_nmp_csv_headers_date_description" => get_string("fml_logs_nmp_csv_headers_date_description","local_fliplearning"),
        "logs_nmp_csv_headers_hour_description" => get_string("fml_logs_nmp_csv_headers_hour_description","local_fliplearning"),
        "logs_nmp_csv_headers_courseid_description" => get_string("fml_logs_nmp_csv_headers_courseid_description","local_fliplearning"),
        "logs_nmp_csv_headers_section_name_description" => get_string("fml_logs_nmp_csv_headers_section_name_description", "local_fliplearning"),
        "logs_nmp_csv_headers_action_type_description" => get_string("fml_logs_nmp_csv_headers_action_type_description", "local_fliplearning"),

        "logs_success_file_downloaded" => get_string("fml_logs_success_file_downloaded", "local_fliplearning"),
        "logs_error_problem_encountered" => get_string("fml_logs_error_problem_encountered", "local_fliplearning"),
        "logs_error_begin_date_superior" => get_string("fml_logs_error_begin_date_superior", "local_fliplearning"),
        "logs_error_begin_date_inferior" => get_string("fml_logs_error_begin_date_inferior", "local_fliplearning"),
        "logs_error_empty_dates" => get_string("fml_logs_error_empty_dates", "local_fliplearning"),

        "logs_moodle_table_title" => get_string("fml_logs_moodle_table_title", "local_fliplearning"),
        "logs_moodle_table_subtitle" => get_string("fml_logs_moodle_table_subtitle", "local_fliplearning"),

        "logs_nmp_table_title" => get_string("fml_logs_nmp_table_title", "local_fliplearning"),
        "logs_nmp_table_subtitle" => get_string("fml_logs_nmp_table_subtitle", "local_fliplearning"),

        "logs_help_button_nmp" => get_string("fml_logs_help_button_nmp", "local_fliplearning"),
        "logs_help_button_moodle" => get_string("fml_logs_help_button_moodle", "local_fliplearning"),

        "logs_download_details_title" => get_string("fml_logs_download_details_title", "local_fliplearning"),
        "logs_download_details_description" => get_string("fml_logs_download_details_title", "local_fliplearning"),
        "logs_download_details_ok" => get_string("fml_logs_download_details_ok", "local_fliplearning"),
        "logs_download_details_cancel" => get_string("fml_logs_download_details_cancel", "local_fliplearning"),
        "logs_download_details_validation" => get_string("fml_logs_download_details_validation", "local_fliplearning"),
        "logs_download_details_link" => get_string("fml_logs_download_details_link", "local_fliplearning"),
    ]
];

$PAGE->requires->js_call_amd('local_fliplearning/logs', 'init', ['content' => $content]);
echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_fliplearning/logs', ['content' => $content]);
echo $OUTPUT->footer();