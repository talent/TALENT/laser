# LASER

LASER(https://www.irit.fr/lase) is a research project funded by the French National Research Agency (ANR) which main objective is to investigate how to better understand and support students’ self-regulated learning (SRL) strategies in BL.